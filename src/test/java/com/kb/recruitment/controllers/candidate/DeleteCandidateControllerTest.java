package com.kb.recruitment.controllers.candidate;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DeleteCandidateControllerTest extends AbstractCandidateControllerTest{

    private String url = "/api/manager/delete-candidate-id";

    @Test
    public void requestWithParamTestShouldReturnBadRequest() throws Exception {
        mockMvc
                .perform(delete(url).param("id", "1"))
                .andDo(print())
                .andExpect(status().isOk());
        verify(candidateService).deleteCandidateById(Mockito.anyLong());
    }

}
