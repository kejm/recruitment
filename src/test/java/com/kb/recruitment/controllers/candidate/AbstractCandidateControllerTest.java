package com.kb.recruitment.controllers.candidate;


import com.kb.recruitment.services.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public abstract class AbstractCandidateControllerTest {

    @Autowired
    protected MockMvc mockMvc;

    @MockBean
    public CandidateService candidateService;


}
