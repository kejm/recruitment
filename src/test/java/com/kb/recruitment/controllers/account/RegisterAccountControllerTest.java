package com.kb.recruitment.controllers.account;

import com.kb.recruitment.entities.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.MediaType;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RegisterAccountControllerTest extends AbstractAccountControllerTest {

    String url = "/acc/add-user";

    @Test
    public void addWithoutJsonBody() throws Exception {
       mockMvc.perform(post(url)
               .contentType(MediaType.APPLICATION_JSON_VALUE))
       .andDo(print())
       .andExpect(status().isBadRequest());
       verify(accountService,never()).addUser(Mockito.any(User.class));
    }


}
