package examples

import com.kb.recruitment.entities.User
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class ExampleAsserationLibrary extends Specification {

    def "Should be able to add to list"() {
        given:
        def list = [1, 2, 3, 4]

        when:
        list.add(4, 6)

        then:
        list == [1, 2, 3, 4, 6]
    }

    def "Should be able to remove from list"() {
        given:
        def list = [1, 2, 3, 4]

        when:
        list.remove(0)

        then:
        list == [2, 3, 4]
    }

    def "Should get an index out of bounds when removing a non-existent item"() {
        given:
        def list = [1, 2, 3, 4]

        when:
        list.remove(20)

        then:
        thrown(IndexOutOfBoundsException)
        list.size() == 4

        cleanup:
        println "Cleanup after test"
    }

    def "Should get index out of bound exception when add unnecessary additional number"() {
        setup:
        def list = [1,2,3,4,5,6]

        when:
        list.remove(5)
        then:
        list.size() == 5

        when:
        list.add(5, 10)
        then:
        list == [1,2,3,4,5,10]

        cleanup:
        println "Cleanup after test"
    }

    def "Wersion II - Should get index out of bound exception when add unnecessary additional number"() {
        setup:
        def list = [1,2,3,4,5,6]

        when:
        list.remove(5)
        list.add(5, 10)

        then:
        list == [1, 2,3,4,5,10]
        and:
        list.size() == 6
        and:
        list.size() == 7-1

        cleanup:
        println "Cleanup after test"
    }

    def "example"() {
        given:
        def stack = new Stack()

        when:
        stack.pop()

        then:
        def e = thrown(EmptyStackException)
        e.cause == null
    }

    def "HashMap accepts null key"() {
        given:
        def map = new HashMap()

        when:
        map.put(null, "elem")

        then:
        notThrown(NullPointerException)
    }

    //works but class Publisher needed
//    def "events are published to all subscribers"() {
//        given:
//        def subscriber1 = Mock(Subscriber)
//        def subscriber2 = Mock(Subscriber)
//        def publisher = new Publisher()
//        publisher.add(subscriber1)
//        publisher.add(subscriber2)
//
//        when:
//        publisher.fire("event")
//
//        then:
//        1 * subscriber1.receive("event")
//        1 * subscriber2.receive("event")
//    }

    //example helper methods
//    def "offered PC matches preferred configuration"() {
//        when:
//        def pc = shop.buyPc()
//
//        then:
//        pc.vendor == "Sunny"
//        pc.clockRate >= 2333
//        pc.ram >= 4096
//        pc.os == "Linux"
//    }
//
//    def "offered PC matches preferred configuration"() {
//        when:
//        def pc = shop.buyPc()
//
//        then:
//        matchesPreferredConfiguration(pc)
//    }
//
//    def matchesPreferredConfiguration(pc) {
//        pc.vendor == "Sunny"
//                && pc.clockRate >= 2333
//                && pc.ram >= 4096
//                && pc.os == "Linux"
//    }
//    void matchesPreferredConfiguration(pc) {
//        assert pc.vendor == "Sunny"
//        assert pc.clockRate >= 2333
//        assert pc.ram >= 4096
//        assert pc.os == "Linux"
//    }

    //Alternative to helper methods using with for expectations
//    def "offered PC matches preferred configuration"() {
//        when:
//        def pc = shop.buyPc()
//
//        then:
//        with(pc) {
//            vendor == "Sunny"
//            clockRate >= 2333
//            ram >= 406
//            os == "Linux"
//        }
//    }




}
