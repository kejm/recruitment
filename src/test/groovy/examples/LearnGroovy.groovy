package examples

import spock.lang.Specification

class LearnGroovy extends Specification {

    def "try something with SPOCK and entity from rest"() {
        given:
        def user = new com.kb.recruitment.entities.User()

        when:
        user.setEmail("example@gmail.com")

        then:
        user.getEmail() == "example@gmail.com"

        cleanup:
        println "Cleanup after test"
    }

    def "#user.name is a #sex.toLowerCase() user"() {
        expect:
        human.getSex() == sex

        where:
            human                || sex
        new Human(name: "John")  || "Male"
        new Human(name: "Wilma") || "Female"
    }

    static class Human {
        String name
        String getSex() {
            name == "John" ? "Male" : "Female"
        }
    }

    def "#person.name is a #sex.toLowerCase() person"() {
        expect:
        person.getSex() == sex

        where:
        person                    || sex
        new Person(name: "Fred")  || "Male"
        new Person(name: "Wilma") || "Female"
    }

    static class Person {
        String name
        String getSex() {
            name == "Fred" ? "Male" : "Female"
        }
    }

}
