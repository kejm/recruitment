package examples

import spock.lang.Specification

class ExampleTest extends Specification{

    def "two plus two should equal four"() {
        //setup can by aliased by given
        setup:
        int left = 2
        int right = 2

        when:
        int result = left + right + 1

        then:
        result != 4
    }

}
