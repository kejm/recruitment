package com.kb.recruitment.services;

import com.kb.recruitment.dtos.AddQuestionDto;
import com.kb.recruitment.entities.Answer;
import com.kb.recruitment.entities.Question;
import com.kb.recruitment.entities.QuestionHasQuiz;
import com.kb.recruitment.exceptions.ApiRequestException;
import com.kb.recruitment.repositories.AnswerRepository;
import com.kb.recruitment.repositories.QuestionHasQuizRepository;
import com.kb.recruitment.repositories.QuestionRepository;
import com.kb.recruitment.dtos.EditQuestionDto;
import com.kb.recruitment.dtos.IdQuestionDto;
import com.kb.recruitment.mappers.AnswerMapper;
import com.kb.recruitment.mappers.QuestionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class QuestionService {

    private QuestionHasQuizRepository questionHasQuizRepository;
    private QuestionRepository questionRepository;
    private AnswerRepository answerRepository;
    private QuestionMapper questionMapper;
    private AnswerMapper answerMapper;

    @Autowired
    public QuestionService(QuestionHasQuizRepository questionHasQuizRepository, QuestionRepository questionRepository, QuestionMapper questionMapper, AnswerRepository answerRepository, AnswerMapper answerMapper) {
        this.questionRepository = questionRepository;
        this.questionHasQuizRepository = questionHasQuizRepository;
        this.questionMapper = questionMapper;
        this.answerRepository = answerRepository;
        this.answerMapper = answerMapper;
    }

    public void editQuestion(EditQuestionDto editQuestionDto){
        Optional<Question> optionalQuestion = questionRepository.findById(editQuestionDto.getId());
        if(optionalQuestion.isPresent()){
            Question question = questionMapper.editQuestionDtoToQuestion(editQuestionDto);
            questionRepository.save(question);
        } else {
            throw new ApiRequestException("We can't find question with this Id!");
        }
    }

    public void addQuestionWithAnswers(AddQuestionDto addQuestionDto) {
        Question question = questionMapper.toEntity(addQuestionDto);
        Long id = questionRepository.save(question).getId();
        List<Answer> answers = answerMapper.toEntity(addQuestionDto.getAnswers());
        answers.stream()
                .peek(a -> a.setQuestion(questionRepository.getQuestionById(id)))
                .collect(Collectors.toList());
        answerRepository.saveAll(answers);
    }

    public void addOpenQuestionWithoutAnswers(AddQuestionDto addQuestionDto){
        Question question = questionMapper.toEntity(addQuestionDto);
        questionRepository.save(question);
    }

    public void deleteQuestionWithAnswers(IdQuestionDto idQuestionDto) {
        try {
            List<QuestionHasQuiz> questionHasQuizs = questionHasQuizRepository.findByQuestionId(idQuestionDto.getQuestionId());
            questionHasQuizRepository.deleteAll(questionHasQuizs);
            List<Answer> answers = answerRepository.findByQuestionId(idQuestionDto.getQuestionId());
            answerRepository.deleteAll(answers);
            questionRepository.deleteById(idQuestionDto.getQuestionId());
        } catch (Exception e) {
            throw new ApiRequestException("We can't find question with this Id!");
        }
    }
}
