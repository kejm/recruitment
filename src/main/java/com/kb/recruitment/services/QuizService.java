package com.kb.recruitment.services;

import com.kb.recruitment.dtos.*;
import com.kb.recruitment.entities.*;
import com.kb.recruitment.exceptions.BreakLinkException;
import com.kb.recruitment.exceptions.DontExistException;
import com.kb.recruitment.exceptions.SomethingWrongException;
import com.kb.recruitment.mappers.*;
import com.kb.recruitment.repositories.*;
import com.kb.recruitment.services.encryptions.EncryptionService;
import com.kb.recruitment.services.encryptions.GenerateService;
import com.kb.recruitment.services.mail.SendEmailSSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class QuizService {

    private QuestionRepository questionRepository;
    private QuizRepository quizRepository;
    private AnswerRepository answerRepository;
    private CandidateRepository candidateRepository;
    private QuestionHasQuizRepository questionHasQuizRepository;
    private CandidateAnswerRepository candidateAnswerRepository;

    private QuestionHasQuizMapper questionHasQuizMapper;
    private CandidateAnswerMapper candidateAnswerMapper;
    private QuestionMapper questionMapper;
    private AnswerMapper answerMapper;
    private QuizMapper quizMapper;
    private EncryptionService encryptionService;

    @Autowired
    public QuizService(AnswerMapper answerMapper, AnswerRepository answerRepository, EncryptionService encryptionService, QuestionRepository questionRepository, QuizRepository quizRepository, CandidateRepository candidateRepository, QuestionHasQuizRepository questionHasQuizRepository, CandidateAnswerRepository candidateAnswerRepository, QuestionHasQuizMapper questionHasQuizMapper, CandidateAnswerMapper candidateAnswerMapper, QuestionMapper questionMapper, QuizMapper quizMapper) {
        this.questionRepository = questionRepository;
        this.quizRepository = quizRepository;
        this.answerRepository = answerRepository;
        this.candidateRepository = candidateRepository;
        this.questionHasQuizRepository = questionHasQuizRepository;
        this.candidateAnswerRepository = candidateAnswerRepository;
        this.questionHasQuizMapper = questionHasQuizMapper;
        this.candidateAnswerMapper = candidateAnswerMapper;
        this.questionMapper = questionMapper;
        this.answerMapper = answerMapper;
        this.quizMapper = quizMapper;
        this.encryptionService = encryptionService;
    }

    protected void sendMail(Candidate candidate, Quiz quiz) {
        try {
            SendEmailSSL sender = new SendEmailSSL();
            sender.sendMail(candidate.getEmail(), "Ivitation to online test","Dear" + candidate.getName() + "\n" + "http://localhost:8080/api/quiz/" + quiz.getLink());
        } catch (Exception e) {
            throw new SomethingWrongException("Mail can't be send!");
        }
    }

    public GenerateQuizDto addQuizWithCustomLinkForCandidate(IdRecruitDto idRecruitDto, BuildQuizDto buildQuizDto){
        Optional<Candidate> optionalCandidate = candidateRepository.findById(idRecruitDto.getRecruitId());
        if (optionalCandidate.isPresent()) {
            Quiz quiz = quizMapper.toEntity(idRecruitDto);
            Long id = quizRepository.save(quiz).getId();

            Candidate candidate = optionalCandidate.get();
            candidate.setQuiz(quizRepository.getQuizById(id));
            candidateRepository.save(candidate);

            String generateLink = GenerateService.randomAlphaNumeric(50);
            quiz.setLink(generateLink);

            List<Question> questions = questionRepository.getRandomQuestions();
            for (Question x : questions) {
                QuestionHasQuiz questionHasQuiz = questionHasQuizMapper.toEntity(buildQuizDto);
                questionHasQuiz.setQuestion(x);
                questionHasQuiz.setQuiz(quiz);
                questionHasQuizRepository.save(questionHasQuiz);
            }

            sendMail(candidate, quiz);

            return new GenerateQuizDto(quiz.getId(), quiz.getLink());
        } else {
            throw new DontExistException("We can't find this candidate!");
        }
    }

    public List<ShowQuestionsDto> linkToQuiz(String link) {
        try {
            Quiz quiz = quizRepository.getQuizByLink(link);
            List<Question> questions = questionRepository.getAllQuestionByQuiz_Id(quiz.getId());
            List<ShowQuestionsDto> showQuestionsDtos = questionMapper.toDto(questions);

            List<ShowQuestionsDto> StreamShowQuestionsDto = showQuestionsDtos.stream()
                    .peek(x -> x.setAnswers(answerMapper.toDto(answerRepository.findByQuestionId(x.getId()))))
                    .collect(Collectors.toList());

            return StreamShowQuestionsDto;
        } catch (Exception e) {
            throw new BreakLinkException("Link looks damaged");
        }
    }

    public void saveCandidateAnswer(AddCandidateAnswerDto addCandidateAnswerDto){
        Candidate candidate = candidateRepository.getCandidateById(addCandidateAnswerDto.getCandidateId());

        CandidateAnswer candidateAnswer = candidateAnswerMapper.toEntity(addCandidateAnswerDto);
        candidateAnswer.setQuiz(candidate.getQuiz());

        Question question = questionRepository.getQuestionById(addCandidateAnswerDto.getQuestionId());
        candidateAnswer.setQuestionContent(question.getContent());
        candidateAnswerRepository.save(candidateAnswer);
    }

    public List<ShowQuestionsDto> showAllQUizQuestion(IdQuizDto idQuizDto){
        List<Question> questions = questionRepository.getAllQuestionByQuiz_Id(idQuizDto.getQuizId());
        List<ShowQuestionsDto> showQuestionsDtos = questionMapper.toDto(questions);

        List<ShowQuestionsDto> StreamShowQuestionsDto = showQuestionsDtos.stream()
                .filter(x -> x.getTechnology().equals("backend"))
                .peek(x -> x.setAnswers(answerMapper.toDto(answerRepository.findByQuestionId(x.getId()))))
                .collect(Collectors.toList());

        return StreamShowQuestionsDto;
    }

}
