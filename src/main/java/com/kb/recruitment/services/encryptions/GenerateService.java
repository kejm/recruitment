package com.kb.recruitment.services.encryptions;

public class GenerateService {

    private static final String ALPHA_NUMERIC_STRING = "qwertyuiopasdfghjklzxcvbnm&=ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

}
