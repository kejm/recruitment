package com.kb.recruitment.services;

import com.kb.recruitment.dtos.AddCandidateDto;
import com.kb.recruitment.dtos.EditCandidateDto;
import com.kb.recruitment.dtos.IdRecruitDto;
import com.kb.recruitment.entities.Candidate;
import com.kb.recruitment.exceptions.DontExistException;
import com.kb.recruitment.exceptions.SomethingWrongException;
import com.kb.recruitment.mappers.CandidateMapper;
import com.kb.recruitment.mappers.UserMapper;
import com.kb.recruitment.repositories.CandidateRepository;
import com.kb.recruitment.repositories.QuizRepository;
import com.kb.recruitment.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CandidateService {

    private UserRepository userRepository;
    private CandidateRepository candidateRepository;
    private UserMapper userMapper;
    private CandidateMapper candidateMapper;
    private QuizRepository quizRepository;

    @Autowired
    public CandidateService(UserRepository userRepository, CandidateRepository candidateRepository, UserMapper userMapper, CandidateMapper candidateMapper, QuizRepository quizRepository) {
        this.userRepository = userRepository;
        this.candidateRepository = candidateRepository;
        this.userMapper = userMapper;
        this.candidateMapper = candidateMapper;
        this.quizRepository = quizRepository;
    }


    protected void addCandidateMethod(AddCandidateDto addCandidateDto){
        Candidate candidate = candidateMapper.toEntity(addCandidateDto);
        candidateRepository.save(candidate);
    }

    public void addCandidate(AddCandidateDto addCandidateDto){
        try {
            addCandidateMethod(addCandidateDto);
        } catch (Exception e) {
            throw new SomethingWrongException();
        }
    }

    public void editCandidate(EditCandidateDto editCandidateDto) {
        try {
            Candidate candidate = candidateMapper.editCandidateDtoToCandidate(editCandidateDto);
            candidateRepository.save(candidate);
        } catch (Exception e) {
            throw new DontExistException("Candidate with this id does't exist");
        }
    }

    public void deleteCandidate(IdRecruitDto idRecruitDto) {
        try {
            Candidate candidate = candidateRepository.getCandidateById(idRecruitDto.getRecruitId());
            candidateRepository.delete(candidate);
        } catch (Exception e) {
            throw new DontExistException("Candidate with this id does't exist");
        }
    }

    public ResponseEntity<String> deleteCandidateById(Long id) {
        candidateRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
