package com.kb.recruitment.services;

import com.kb.recruitment.dtos.ChangePasswordDto;
import com.kb.recruitment.dtos.LoginDto;
import com.kb.recruitment.dtos.RegisterDto;
import com.kb.recruitment.dtos.TokenDto;
import com.kb.recruitment.entities.User;
import com.kb.recruitment.exceptions.AlreadyExistExceptions;
import com.kb.recruitment.exceptions.ApiRequestException;
import com.kb.recruitment.exceptions.DontExistException;
import com.kb.recruitment.exceptions.IncorrectPasswordException;
import com.kb.recruitment.mappers.UserMapper;
import com.kb.recruitment.repositories.UserRepository;
import com.kb.recruitment.security.SecurityClaims;
import com.kb.recruitment.security.TokenGenerator;
import com.kb.recruitment.services.encryptions.EncryptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    private UserRepository userRepository;
    private UserMapper userMapper;
    private EncryptionService encryptionService;

    @Autowired
    public AccountService(UserRepository userRepository, UserMapper userMapper, EncryptionService encryptionService) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.encryptionService = encryptionService;
    }

    public boolean arePasswordsTheSame(String password, String repeatPassword){
        return password.equals(repeatPassword);
    }

    public User addUser(User user) {
        return userRepository.save(user);
    }

    public ResponseEntity<String> register(RegisterDto registerDto){
        List<User> users = userRepository.getByEmail(registerDto.getEmail());
        if(users.isEmpty()){
            if(arePasswordsTheSame(registerDto.getPassword(), registerDto.getRepeatPassword())){
                User user = userMapper.toEntity(registerDto);
                user.setPassword(encryptionService.hashPassword(registerDto.getPassword()));
                userRepository.save(user);
                return ResponseEntity.status(HttpStatus.CREATED).body("Register successfully ended!");
            }else{
                throw new ApiRequestException("Passwords aren't the same!");
            }
        }else{
            throw new AlreadyExistExceptions("This email is already use");
        }
    }

    public TokenDto login(LoginDto loginDto, HttpServletResponse response) throws AuthenticationException {
        User user = userRepository.getUserByEmail(loginDto.getEmail());
        if (encryptionService.checkPassword(loginDto.getPassword(), user.getPassword())) {
            TokenGenerator token = new TokenGenerator();
            String generatedToken = token.generate(user);
            response.addHeader(SecurityClaims.HEADER_STRING, SecurityClaims.TOKEN_PREFIX + generatedToken);
            return new TokenDto(generatedToken);
        } else {
            throw new AuthenticationException("Authentication failed!");
        }
    }

    public void changePassword(ChangePasswordDto changePasswordDto) {
        Optional<User> optionalUsers = userRepository.findById(changePasswordDto.getId());
        if(optionalUsers.isPresent()){
            User user = optionalUsers.get();
            if(encryptionService.checkPassword(changePasswordDto.getCurrentPassword(), user.getPassword())){
                if(arePasswordsTheSame(changePasswordDto.getNewPassword(), changePasswordDto.getRepeatNewPassword())){
                    user.setPassword(encryptionService.hashPassword(changePasswordDto.getNewPassword()));
                    userRepository.save(user);
                }else{
                    throw new IncorrectPasswordException("Passwords aren't the same!");
                }
            }else{
                throw new IncorrectPasswordException("It's not your password!");
            }
        }else {
            throw new DontExistException("User don't exist");
        }
    }

}
