package com.kb.recruitment.services;

import com.kb.recruitment.dtos.IdRecruitDto;
import com.kb.recruitment.dtos.RatingAnswerDto;
import com.kb.recruitment.dtos.RatingQuizDto;
import com.kb.recruitment.dtos.ShowCandidateAnswerDto;
import com.kb.recruitment.entities.CandidateAnswer;
import com.kb.recruitment.entities.Quiz;
import com.kb.recruitment.exceptions.DontExistException;
import com.kb.recruitment.mappers.CandidateAnswerMapper;
import com.kb.recruitment.projections.LogingUserProjection;
import com.kb.recruitment.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RatingService {

    private UserRepository userRepository;
    private QuizRepository quizRepository;
    private CandidateRepository candidateRepository;
    private CandidateAnswerRepository candidateAnswerRepository;
    private CandidateAnswerMapper candidateAnswerMapper;
    private QuestionRepository questionRepository;

    @Autowired
    public RatingService(CandidateRepository candidateRepository, UserRepository userRepository, QuizRepository quizRepository, CandidateAnswerRepository candidateAnswerRepository, CandidateAnswerMapper candidateAnswerMapper, QuestionRepository questionRepository) {
        this.userRepository = userRepository;
        this.candidateRepository = candidateRepository;
        this.quizRepository = quizRepository;
        this.candidateAnswerRepository = candidateAnswerRepository;
        this.candidateAnswerMapper = candidateAnswerMapper;
        this.questionRepository = questionRepository;
    }

    public LogingUserProjection findUserById(Long id) {
        return userRepository.getUserById(id);
    }

    public List<ShowCandidateAnswerDto> showCandidateAnswers(IdRecruitDto idRecruitDto) {
            List<CandidateAnswer> candidateAnswers = candidateAnswerRepository.getAllCandidateAnswerByCandidateId(idRecruitDto.getRecruitId());
            List<ShowCandidateAnswerDto> showCandidateAnswerDtos = candidateAnswerMapper.toDto(candidateAnswers);
            return showCandidateAnswerDtos;
    }

    public void answerRating(RatingAnswerDto ratingAnswerDto) {
        Optional<CandidateAnswer> optionalCandidateAnswer = candidateAnswerRepository.findById(ratingAnswerDto.getId());
        if (optionalCandidateAnswer.isPresent()) {
            CandidateAnswer candidateAnswer = candidateAnswerMapper.ratingAnswerDtoToEntity(ratingAnswerDto);
            candidateAnswerRepository.save(candidateAnswer);
        } else {
            throw new DontExistException("We can't find this answer");
        }
    }

    public void quizRating(RatingQuizDto ratingQuizDto) {
        Quiz quiz = quizRepository.getQuizById(ratingQuizDto.getQuizId());
        quiz.setComment(ratingQuizDto.getComment());
        quiz.setRate(ratingQuizDto.getRate());
        quizRepository.save(quiz);
    }
}
