package com.kb.recruitment.dtos;

import lombok.Data;

@Data
public class EditQuestionDto extends AbstractDto{

    private String content;
    private String typeQuestion;
    private String technology;
    private String framework;
    private String language;
    private String levelDifficulty;

}
