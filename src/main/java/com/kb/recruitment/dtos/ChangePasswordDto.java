package com.kb.recruitment.dtos;

import com.kb.recruitment.entities.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ChangePasswordDto extends AbstractEntity {

    private String currentPassword;
    private String newPassword;
    private String repeatNewPassword;

}
