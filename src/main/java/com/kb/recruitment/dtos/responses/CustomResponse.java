package com.kb.recruitment.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
public class CustomResponse {

    private String message;
    private HttpStatus httpStatus;
    private String  httpStatusToString;
    private String httpEntityToString;
    private Integer objectHashCode;

    private HttpHeaders header;
    private HttpHeaders  headerMaxAge;

//    private WebRequest request;
//    private String error;
//    private Throwable status;
}
