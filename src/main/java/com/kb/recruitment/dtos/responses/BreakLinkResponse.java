package com.kb.recruitment.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
public class BreakLinkResponse {

    private ZonedDateTime timestamp;
    private String status;
    private String message;
}
