package com.kb.recruitment.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
public class ApiGenerallyResponse<T, S> {

    private ZonedDateTime timestamp;
    private String httpStatus;
    private S message;
    private T status;
}
