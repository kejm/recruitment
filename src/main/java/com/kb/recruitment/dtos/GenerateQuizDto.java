package com.kb.recruitment.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GenerateQuizDto {

    private Long quizId;
    private String link;

}
