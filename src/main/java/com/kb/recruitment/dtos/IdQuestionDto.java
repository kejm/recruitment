package com.kb.recruitment.dtos;

import lombok.Data;

@Data
public class IdQuestionDto {

    private Long questionId;

}
