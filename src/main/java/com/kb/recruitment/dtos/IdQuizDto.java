package com.kb.recruitment.dtos;

import lombok.Data;

@Data
public class IdQuizDto {

    private Long quizId;

}
