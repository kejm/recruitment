package com.kb.recruitment.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class AddCandidateDto{

    private String name;
    private String surname;
    private String email;
    private String coordinator;
    private String recruiter;
    private String curriculum;
    private String technology;
    private Date registerData;
    private String rating;
    private String level;
    private String linkQuiz;
    private String status;

}
