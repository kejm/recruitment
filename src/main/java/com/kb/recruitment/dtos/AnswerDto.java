package com.kb.recruitment.dtos;

import lombok.Data;

@Data
public class AnswerDto extends AbstractDto {

    private String content;
    private boolean correct;

}
