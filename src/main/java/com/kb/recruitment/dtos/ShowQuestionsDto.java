package com.kb.recruitment.dtos;

import lombok.Data;

import java.util.List;

@Data
public class ShowQuestionsDto extends AbstractDto {

    private String content;
    private String typeQuestion;
    private String technology;
    private String framework;
    private String language;
    private String levelDifficulty;
    private List<AnswerDto> answers;

}
