package com.kb.recruitment.dtos;

import com.kb.recruitment.entities.Question;
import lombok.Data;

import java.util.List;

@Data
public class QuestionRecruitDto {

    private List<Question> question;

}
