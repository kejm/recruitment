package com.kb.recruitment.dtos;

import lombok.Data;

@Data
public class RatingQuizDto {

    private Long quizId;
    private String comment;
    private String rate;

}
