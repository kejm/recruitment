package com.kb.recruitment.dtos;

import lombok.Data;

@Data
public class EditCandidateDto extends AbstractDto {

    private String name;
    private String surname;
    private String email;
    private String coordinator;
    private String recruiter;
    private String curriculum;
    private String technology;
    private String level;
    private String status;

}
