package com.kb.recruitment.dtos;

import lombok.Data;

@Data
public class IdRecruitDto {

    private Long recruitId;

}
