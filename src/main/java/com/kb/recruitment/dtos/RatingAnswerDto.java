package com.kb.recruitment.dtos;

import lombok.Data;

@Data
public class RatingAnswerDto extends AbstractDto {

    private String comment;
    private String rate;

}
