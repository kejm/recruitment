package com.kb.recruitment.dtos;

import lombok.Data;

@Data
public class ShowCandidateAnswerDto {

    private Long questionId;
    private String questionContent;
    private String answerForQuestion;

}
