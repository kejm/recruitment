package com.kb.recruitment.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegisterDto {

    private String name;
    private String surname;
    private String password;
    private String repeatPassword;
    private String email;
    private String employeeType;

}
