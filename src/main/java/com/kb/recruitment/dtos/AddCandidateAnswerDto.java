package com.kb.recruitment.dtos;

import lombok.Data;

@Data
public class AddCandidateAnswerDto extends AbstractDto{

    private String answerForQuestion;
    private Long candidateId;
    private Long questionId;

}
