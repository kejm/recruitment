package com.kb.recruitment.projections;

public interface LogingUserProjection {

    String getName();
    String getSurname();
    String getEmail();

}
