package com.kb.recruitment.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.kb.recruitment.entities.User;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

public class TokenGenerator {

    public String generate(User user) {
        String token = JWT.create()
                .withSubject(user.getEmail())
                .withClaim("id", user.getId())
                .sign(Algorithm.HMAC512(SecurityClaims.SECRET.getBytes()));
        return token;
    }

//    public String generateLink(Candidate candidate) {
//        String link = JWT.create()
//                .withSubject(candidate.getName())
//                .withClaim("id", candidate.getId())
//                .sign(HMAC512(LINK.getBytes()));
//        return link;
//    }

}
