package com.kb.recruitment.security;

public class SecurityClaims {
    public static final String SECRET = "t3rc3s_3rus43m-random_part-8F7hS6fnS689asdmC8fopqmcnhkOpl09278dnbayskm";
    public static final String LINK = "linkToYourQuiz";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String REGISTER_URL = "/acc/register";
    public static final String LOGIN_URL = "/acc/login";
}

