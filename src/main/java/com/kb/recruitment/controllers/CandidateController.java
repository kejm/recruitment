package com.kb.recruitment.controllers;

import com.kb.recruitment.dtos.AddCandidateDto;
import com.kb.recruitment.dtos.EditCandidateDto;
import com.kb.recruitment.dtos.IdRecruitDto;
import com.kb.recruitment.services.CandidateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/manager")
public class CandidateController {

    private final CandidateService candidateService;

    @PostMapping("/add-candidate")
    public void addCandidate(@RequestBody AddCandidateDto addCandidateDto){
        candidateService.addCandidate(addCandidateDto);
    }

    @PutMapping("/edit-candidate")
    public void editCandidate(@RequestBody EditCandidateDto editCandidateDto){
        candidateService.editCandidate(editCandidateDto);
    }

    @DeleteMapping("/delete-candidate")
    public void deleteCandidate(@RequestBody IdRecruitDto idRecruitDto){
        candidateService.deleteCandidate(idRecruitDto);
    }

    @DeleteMapping(path = "/delete-candidate-id", params = "id")
    public ResponseEntity<String> deleteCandidateById(@RequestParam Long id){
        candidateService.deleteCandidateById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
