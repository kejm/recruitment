package com.kb.recruitment.controllers;

import com.kb.recruitment.dtos.*;
import com.kb.recruitment.services.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/quiz")
public class QuizController {

    private QuizService quizService;

    @Autowired
    public QuizController(QuizService quizService) {
        this.quizService = quizService;
    }

    @PutMapping("/add-quiz")
    public GenerateQuizDto addQuizAndSendLinkForCandidate(@RequestBody IdRecruitDto idRecruitDto, BuildQuizDto buildQuizDto){
        return quizService.addQuizWithCustomLinkForCandidate(idRecruitDto, buildQuizDto);
    }

    @GetMapping("/{link}")
    public List<ShowQuestionsDto> quizLink(@PathVariable String link){
        return quizService.linkToQuiz(link);
    }

    @PostMapping("/save-candidate-answer")
    public void saveCandidateAnswer(@RequestBody AddCandidateAnswerDto addCandidateAnswerDto){
        quizService.saveCandidateAnswer(addCandidateAnswerDto);
    }

    @PostMapping("/show-quiz")
    public List<ShowQuestionsDto> showAllQUizQuestion(@RequestBody IdQuizDto idQuizDto){
        return quizService.showAllQUizQuestion(idQuizDto);
    }

}
