package com.kb.recruitment.controllers;

import com.kb.recruitment.dtos.ChangePasswordDto;
import com.kb.recruitment.dtos.LoginDto;
import com.kb.recruitment.dtos.RegisterDto;
import com.kb.recruitment.dtos.TokenDto;
import com.kb.recruitment.entities.User;
import com.kb.recruitment.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletResponse;
import java.awt.print.Book;

@RestController
@RequestMapping("/acc")
public class AccountController {

    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/add-user")
    public ResponseEntity<String> addUser(@RequestBody User user){
        accountService.addUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).body("User successfully added !");
    }

    @PostMapping("/register")
    public void register(@RequestBody RegisterDto registerDto){
        accountService.register(registerDto);
    }

    @PostMapping("/login")
    public TokenDto login(@RequestBody LoginDto loginDto, HttpServletResponse response) throws AuthenticationException {
       return accountService.login(loginDto, response);
    }

    @PutMapping("/change-password")
    public void changePassword(@RequestBody ChangePasswordDto changePasswordDto){
        accountService.changePassword(changePasswordDto);
    }

}
