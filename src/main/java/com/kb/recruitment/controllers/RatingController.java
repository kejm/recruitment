package com.kb.recruitment.controllers;

import com.kb.recruitment.dtos.IdRecruitDto;
import com.kb.recruitment.dtos.RatingAnswerDto;
import com.kb.recruitment.dtos.RatingQuizDto;
import com.kb.recruitment.dtos.ShowCandidateAnswerDto;
import com.kb.recruitment.projections.LogingUserProjection;
import com.kb.recruitment.services.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/user")
public class RatingController {

    private RatingService ratingService;

    @Autowired
    public RatingController(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @GetMapping("/{id}")
    public LogingUserProjection findUserById(@PathVariable Long id){
        return ratingService.findUserById(id);
    }

    @PostMapping("/show-candidate-answers")
    public List<ShowCandidateAnswerDto> showCandidateAnswers(@RequestBody IdRecruitDto idRecruitDto){
        return ratingService.showCandidateAnswers(idRecruitDto);
    }

    @PutMapping("/rating-answer-candidate")
    public void answerRating(@RequestBody RatingAnswerDto ratingAnswerDto) {
        ratingService.answerRating(ratingAnswerDto);
    }

    @PutMapping("/rating-quiz-candidate")
    public void quizRating(@RequestBody RatingQuizDto ratingQuizDto) {
        ratingService.quizRating(ratingQuizDto);
    }

}
