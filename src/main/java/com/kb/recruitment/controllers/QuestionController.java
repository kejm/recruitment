package com.kb.recruitment.controllers;

import com.kb.recruitment.dtos.AddQuestionDto;
import com.kb.recruitment.dtos.EditQuestionDto;
import com.kb.recruitment.dtos.IdQuestionDto;
import com.kb.recruitment.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/manager")
public class QuestionController {

    private QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @PutMapping(value = "/edit-question")
    public void editQuestion(@RequestBody EditQuestionDto editQuestionDto){
        questionService.editQuestion(editQuestionDto);
    }

    @PostMapping(value = "/add-question")
    public void addQuestionWithAnswers(@RequestBody AddQuestionDto addQuestionDto){
        questionService.addQuestionWithAnswers(addQuestionDto);
    }

    @PostMapping(value = "/add-open-question")
    public void addOpenQuestionWithoutAnswers(@RequestBody AddQuestionDto addQuestionDto){
        questionService.addOpenQuestionWithoutAnswers(addQuestionDto);
    }

    @DeleteMapping("/delete-question")
    public void deleteQuestionWithAnswers(@RequestBody IdQuestionDto idQuestionDto) {
        questionService.deleteQuestionWithAnswers(idQuestionDto);
    }

}
