package com.kb.recruitment.exceptions;

public class SomethingWrongException extends RuntimeException {

    public SomethingWrongException() {
    }

    public SomethingWrongException(String message) {
        super(message);
    }

    public SomethingWrongException(String message, Throwable cause) {
        super(message, cause);
    }
}