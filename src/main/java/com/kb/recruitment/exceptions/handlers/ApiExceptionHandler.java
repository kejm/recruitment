package com.kb.recruitment.exceptions.handlers;

import com.kb.recruitment.dtos.responses.ApiGenerallyResponse;
import com.kb.recruitment.dtos.responses.BreakLinkResponse;
import com.kb.recruitment.dtos.responses.CustomResponse;
import com.kb.recruitment.exceptions.ApiRequestException;
import com.kb.recruitment.exceptions.BreakLinkException;
import com.kb.recruitment.exceptions.DontExistException;
import com.kb.recruitment.exceptions.SomethingWrongException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.apache.logging.log4j.core.impl.ThrowableFormatOptions.MESSAGE;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ApiRequestException.class})
    public ResponseEntity<Object> handleApiRequestException(ApiRequestException e) {
        //Create payload containing exception details
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        String status = badRequest.toString();

        ApiGenerallyResponse<HttpStatus, String> apiGenerallyResponse = new ApiGenerallyResponse<>(
                ZonedDateTime.now(ZoneId.of("Z")),
                status,
                e.getMessage(),
                badRequest
        );
        //Return response entity
        logger.error("Something generally went wrong!");
        return new ResponseEntity<>(apiGenerallyResponse, badRequest);
    }

    @ExceptionHandler(value = {BreakLinkException.class})
    public ResponseEntity<Object> handleBreakLinkException(BreakLinkException e) {
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        String status = HttpStatus.BAD_REQUEST.toString();

        HttpHeaders header = new HttpHeaders();
        header.set("Something went wrong!", "Object probably does not exist");

        BreakLinkResponse  breakLinkResponse = new BreakLinkResponse(
                ZonedDateTime.now(ZoneId.of("Z")),
                status,
                e.getMessage()
        );

        logger.error("Bad link to quiz");
        return new ResponseEntity<>(breakLinkResponse, header, httpStatus);
    }

    @ExceptionHandler(value = {SomethingWrongException.class})
    public ResponseEntity<Object> handleSomethingWrongException(RuntimeException e, WebRequest request) {
        String bodyOfResponse = "Something went wrong!";
//        CacheControl cacheControl = new CacheControl().setCachePeriod(31556926);
        HttpHeaders header = new HttpHeaders();
        header.set(MESSAGE, bodyOfResponse);

        HttpStatus status = HttpStatus.BAD_REQUEST;
        String statusString = status.toString();

        ZonedDateTime zone = ZonedDateTime.now(ZoneId.of("Z"));

        ApiGenerallyResponse<HttpStatus, String> apiGenerallyResponse = new ApiGenerallyResponse<>(
                zone,
                statusString,
                e.getMessage(),
                status
        );

        logger.error("Something went wrong!");
        return handleExceptionInternal(e, apiGenerallyResponse, header, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = {DontExistException.class})
    public ResponseEntity<Object> handleDontExistException(DontExistException e) {
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        String status = HttpStatus.BAD_REQUEST.toString();

        HttpHeaders header = new HttpHeaders();
        header.set("Something went wrong!", "Object probably does not exist");

        HttpHeaders maxAge = new HttpHeaders();
        maxAge.setAccessControlMaxAge(18);

        CustomResponse customResponse = new CustomResponse(
                e.getMessage(),
                httpStatus,
                status,
                e.toString(),
                e.hashCode(),
                header,
                maxAge);

        logger.error("Object with this id does't exist");
        return new ResponseEntity<>(customResponse, header, httpStatus);
    }
}