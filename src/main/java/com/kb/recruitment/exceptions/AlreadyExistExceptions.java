package com.kb.recruitment.exceptions;

public class AlreadyExistExceptions extends RuntimeException {
    public AlreadyExistExceptions(String message){
        super(message);
    }
}

