package com.kb.recruitment.exceptions;

public class BreakLinkException extends RuntimeException{

    public BreakLinkException() {
    }

    public BreakLinkException(String message) {
        super(message);
    }
}
