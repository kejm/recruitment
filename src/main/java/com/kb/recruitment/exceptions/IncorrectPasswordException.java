package com.kb.recruitment.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IncorrectPasswordException extends RuntimeException {
    public IncorrectPasswordException (String message) {
        super(message);
    }
}