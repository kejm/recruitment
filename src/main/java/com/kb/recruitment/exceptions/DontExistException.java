package com.kb.recruitment.exceptions;

public class DontExistException extends RuntimeException {

    public DontExistException() {
    }

    public DontExistException(String message) {
        super(message);
    }
}