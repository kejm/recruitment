package com.kb.recruitment.repositories;

import com.kb.recruitment.entities.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

    Question getQuestionById(Long id);

    @Query(value = "SELECT * FROM question ORDER BY RAND() LIMIT 20;", nativeQuery = true)
    @Transactional
    List<Question> getRandomQuestions();

    @Query(value = "SELECT * FROM question JOIN question_has_quiz ON question.id = question_has_quiz.question_id WHERE question_has_quiz.quiz_id = :quizId", nativeQuery = true)
    @Modifying
    @Transactional
    List<Question> getAllQuestionByQuiz_Id(@Param("quizId") Long id);

}
