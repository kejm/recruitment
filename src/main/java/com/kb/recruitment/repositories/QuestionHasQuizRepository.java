package com.kb.recruitment.repositories;

import com.kb.recruitment.entities.QuestionHasQuiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionHasQuizRepository extends JpaRepository<QuestionHasQuiz, Long> {

    List<QuestionHasQuiz> findByQuestionId(Long id);

}
