package com.kb.recruitment.repositories;

import com.kb.recruitment.entities.Candidate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long> {

    Candidate getCandidateById(Long id);

}
