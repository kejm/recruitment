package com.kb.recruitment.repositories;

import com.kb.recruitment.entities.User;
import com.kb.recruitment.projections.LogingUserProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> getByEmail(String email);
    User getUserByEmail(String email);
    LogingUserProjection getUserById(Long id);

//    User getUserById(Long id);

}
