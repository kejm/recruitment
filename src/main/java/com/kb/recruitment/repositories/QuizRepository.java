package com.kb.recruitment.repositories;

import com.kb.recruitment.entities.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuizRepository extends JpaRepository<Quiz, Long> {

    Quiz getQuizById(Long id);
    Quiz getQuizByLink(String link);

}
