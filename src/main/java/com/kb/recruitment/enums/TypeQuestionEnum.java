package com.kb.recruitment.enums;

public enum TypeQuestionEnum {

    OPEN("otwarte"),
    CLOSE("zamknięte");

    private String name;

    TypeQuestionEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}

