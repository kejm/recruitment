package com.kb.recruitment.enums;

public enum TechnologyEnum {

    BACKEND,
    FRONTEND,
    SALESFORCE;

}
