package com.kb.recruitment.mappers;

import com.kb.recruitment.dtos.AnswerDto;
import com.kb.recruitment.entities.Answer;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, componentModel = "spring")
public interface AnswerMapper {

    Answer toEntity(AnswerDto answerDto);

    List<Answer> toEntity(List<AnswerDto> answerDto);
    List<AnswerDto> toDto(List<Answer> answers);

}
