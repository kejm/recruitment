package com.kb.recruitment.mappers;

import com.kb.recruitment.dtos.AddCandidateAnswerDto;
import com.kb.recruitment.dtos.RatingAnswerDto;
import com.kb.recruitment.dtos.ShowCandidateAnswerDto;
import com.kb.recruitment.entities.CandidateAnswer;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, componentModel = "spring")
public interface CandidateAnswerMapper {

    CandidateAnswer ratingAnswerDtoToEntity(RatingAnswerDto ratingAnswerDto);
    CandidateAnswer toEntity(AddCandidateAnswerDto addCandidateAnswerDto);
    List<ShowCandidateAnswerDto> toDto(List<CandidateAnswer> candidateAnswer);

}
