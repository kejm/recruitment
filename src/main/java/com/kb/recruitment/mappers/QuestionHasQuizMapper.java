package com.kb.recruitment.mappers;

import com.kb.recruitment.dtos.BuildQuizDto;
import com.kb.recruitment.entities.QuestionHasQuiz;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, componentModel = "spring")
public interface QuestionHasQuizMapper {

    QuestionHasQuiz toEntity(BuildQuizDto buildQuizDto);

}
