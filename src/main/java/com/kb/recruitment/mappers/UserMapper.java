package com.kb.recruitment.mappers;

import com.kb.recruitment.dtos.RegisterDto;
import com.kb.recruitment.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface UserMapper {

    User toEntity(RegisterDto registerDto);

}
