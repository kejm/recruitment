package com.kb.recruitment.mappers;

import com.kb.recruitment.dtos.AddQuestionDto;
import com.kb.recruitment.dtos.EditQuestionDto;
import com.kb.recruitment.dtos.ShowQuestionsDto;
import com.kb.recruitment.entities.Question;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, componentModel = "spring")
public interface QuestionMapper {

    Question toEntity(AddQuestionDto addQuestionDto);
    Question editQuestionDtoToQuestion(EditQuestionDto editQuestionDto);
    ShowQuestionsDto questionToShowQuestionDto(Question question);

    List<ShowQuestionsDto> toDto(List<Question> questions);

}
