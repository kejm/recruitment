package com.kb.recruitment.mappers;

import com.kb.recruitment.dtos.IdRecruitDto;
import com.kb.recruitment.entities.Quiz;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, componentModel = "spring")
public interface QuizMapper {

    Quiz toEntity(IdRecruitDto idRecruitDto);

}
