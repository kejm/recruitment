package com.kb.recruitment.mappers;

import com.kb.recruitment.dtos.AddCandidateDto;
import com.kb.recruitment.dtos.EditCandidateDto;
import com.kb.recruitment.entities.Candidate;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, componentModel = "spring")
public interface CandidateMapper {

    Candidate toEntity(AddCandidateDto addCandidateDto);
    Candidate editCandidateDtoToCandidate(EditCandidateDto editCandidateDto);

}
