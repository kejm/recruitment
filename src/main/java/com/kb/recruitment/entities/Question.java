package com.kb.recruitment.entities;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class Question extends AbstractEntity {

    private String content;
    private String typeQuestion;
    private String technology;
    private String framework;
    private String language;
    private String levelDifficulty;

}
