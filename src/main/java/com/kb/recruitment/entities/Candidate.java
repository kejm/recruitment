package com.kb.recruitment.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
@Entity
public class Candidate extends AbstractEntity {

    private String name;
    private String surname;
    private String email;
    private String coordinator;
    private String recruiter;
    private String curriculum;
    private String technology;
    private String level;
    private Date registerData;
    private String status;
    private String comment;
    private String endRate;

    @ManyToOne
    @JoinColumn(name = "quiz_id")
    private Quiz quiz;

}