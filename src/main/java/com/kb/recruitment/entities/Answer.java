package com.kb.recruitment.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@Entity
public class Answer extends AbstractEntity {

    private String content;
    private boolean correct;

    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;

}
