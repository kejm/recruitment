package com.kb.recruitment.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@Entity
public class CandidateAnswer extends AbstractEntity {

    private String answerForQuestion;
    private String typeQuestion;
    private String questionContent;
    private String comment;
    private String rate;

    private Long candidateId;
    private Long questionId;

    @ManyToOne
    @JoinColumn(name = "quiz_id")
    private Quiz quiz;
}
