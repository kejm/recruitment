package com.kb.recruitment.entities;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class User extends AbstractEntity {

    private String name;
    private String surname;
    private String password;
    private String email;
    private String employeeType;

}
