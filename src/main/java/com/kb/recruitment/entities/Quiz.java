package com.kb.recruitment.entities;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class Quiz extends AbstractEntity {

    private String link;
    private String comment;
    private String rate;

}
